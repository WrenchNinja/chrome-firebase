(function () {
	'use strict';

	Firebase.INTERNAL.forceWebSockets(); 
	var myFirebaseRef = new Firebase("https://popping-inferno-9926.firebaseio.com/");

	/*
	myFirebaseRef.set({
		chats: {
			chat1: {
				id: 1,
				name: "kali",
				lastText: "Ik praat gewoon"
			},
			chat2: {
				id: 2,
				name: "Demi",
				lastText: "Ik ben nog steeds benieuwd wat we eten"
			},
			chat3: {
				id: 3,
				name: "Johan",
				lastText: "Ga je me nog verwijderen?"
			}
		},
		highscores: {
			asfdlkjasf: {
				name: "WrenchNinja",
				score: 1000
			},
			fasdfasdf: {
				name: "CurlyChick",
				score: 10234
			}
		}
	});
	*/

	var username = "CurlyChick";
	var firebaseUsernames = new Firebase("https://popping-inferno-9926.firebaseio.com/usernames/" + username);

	firebaseUsernames.set({
		name: "Demi",
		age: 23,
		gender: "female"
	});

	myFirebaseRef.child("chats/chat1/name").on("value", function(snapshot) {
	 	var temp = snapshot.val();
		var node = document.createElement("LI");
		var textnode = document.createTextNode(temp);
		node.appendChild(textnode);
		document.getElementById("firebase-population").appendChild(node);
	});
	
})();